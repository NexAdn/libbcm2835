#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "bcm2835/bcm2835_gpio.h"

#include "bcm2835/bcm2835_peripheral.h"

typedef volatile uint32_t* register_ptr_t;

static int get_function_select_bits(BCM2835_GPIO_FUNCTION function);
static int get_function_register_number(int pin_number);
static inline int get_bit_offset_inside_register(int pin, int register_no);
static void write_function_select_at_offset(register_ptr_t memory_begin, int register_no,
                                            int offset_bits, int fsel);
static inline uint32_t get_register_at_offset(register_ptr_t base_register, int register_offset);
static inline void clock_pull_up_down_register(register_ptr_t pud_clock_base_register, int pin);
static inline void set_register_bit_at_offset(register_ptr_t base_register, int register_offset,
                                              int bit_offset);
static inline void clear_register_bit_at_offset(register_ptr_t base_register, int register_offset,
                                                int bit_offset);
static inline int get_common_register_offset(int pin);
static inline int get_common_register_bit_offset(int pin);
static void wait_n_clock_ticks(unsigned int n_ticks);

static unsigned int PUDCLK_WAIT_TICKS = 150;

bcm2835_gpio* bcm2835_gpio_create()
{
    bcm2835_gpio* this = malloc(sizeof(bcm2835_gpio));
    this->peripheral = bcm2835_peripheral_create();

    return this;
}

int bcm2835_gpio_open(bcm2835_gpio* this)
{
    if (bcm2835_peripheral_open(this->peripheral) < 0) {
        fprintf(stderr, "bcm2835_gpio_open(): Failed to open peripheral\n");
        return -1;
    }
    this->memory = (volatile bcm2835_gpio_memory*) this->peripheral->addr;

    return 0;
}

void bcm2835_gpio_close(bcm2835_gpio* this)
{
    // TODO
}

void bcm2835_gpio_destroy(bcm2835_gpio* this)
{
    bcm2835_peripheral_destroy(this->peripheral);
    free(this);
}

void bcm2835_gpio_select_function(bcm2835_gpio* this, int pin, BCM2835_GPIO_FUNCTION function)
{
    int f_sel = get_function_select_bits(function);

    const int register_no = get_function_register_number(pin);
    const int pos_inside_register = get_bit_offset_inside_register(pin, register_no);

    write_function_select_at_offset(&(this->memory->gpfsel0), register_no, pos_inside_register,
                                    f_sel);
}

static inline int get_function_select_bits(BCM2835_GPIO_FUNCTION function)
{
    return (function & 0x7);
}

static inline int get_function_register_number(int pin_number)
{
    static const int BITS_PER_FUNCTION_REGISTER = 30;
    int begin_bit_no = pin_number * 3;
    int begin_bit_in_register = begin_bit_no % 30;
    int register_number = (begin_bit_no - begin_bit_in_register) / BITS_PER_FUNCTION_REGISTER;
    return register_number;
}

static inline int get_bit_offset_inside_register(int pin, int register_no)
{
    return (pin * 3 - 30 * register_no);
}

static inline void write_function_select_at_offset(register_ptr_t memory_begin, int register_no,
                                                   int offset_bits, int fsel)
{
    *(memory_begin + register_no) &= ~(0x7 << (offset_bits));
    *(memory_begin + register_no) |= (fsel << (offset_bits));
}

void bcm2835_gpio_set_output(bcm2835_gpio* this, int pin, BCM2835_GPIO_LEVEL level)
{
    if (level != 0) {
        bcm2835_gpio_set_output_high(this, pin);
    } else {
        bcm2835_gpio_set_output_low(this, pin);
    }
}

void bcm2835_gpio_set_output_high(bcm2835_gpio* this, int pin)
{
    register_ptr_t first_register = &this->memory->gpset0;
    int register_offset = get_common_register_offset(pin);
    int bit_offset = get_common_register_offset(pin);
    set_register_bit_at_offset(first_register, register_offset, bit_offset);
}

void bcm2835_gpio_set_output_low(bcm2835_gpio* this, int pin)
{
    register_ptr_t first_register = &this->memory->gpclr0;
    int register_offset = get_common_register_offset(pin);
    int bit_offset = get_common_register_offset(pin);
    set_register_bit_at_offset(first_register, register_offset, bit_offset);
}

BCM2835_GPIO_LEVEL bcm2835_gpio_get_input(bcm2835_gpio* this, int pin)
{
    int input_register_offset = get_common_register_offset(pin);
    int register_value = get_register_at_offset(&this->memory->gplev0, input_register_offset);
    int register_bit_offset = get_common_register_bit_offset(pin);
    int register_bit_value = register_value & (1 << register_bit_offset);
    return register_bit_value >> register_bit_offset;
}

static inline uint32_t get_register_at_offset(register_ptr_t base_register, int register_offset)
{
    return *(base_register + register_offset);
}

void bcm2835_gpio_set_pull_up(bcm2835_gpio* this, int pin)
{
    register_ptr_t pud_register = &this->memory->gppud;
    register_ptr_t first_pud_clock_register = &this->memory->gppudclk0;

    set_register_bit_at_offset(pud_register, 0, 1);
    clock_pull_up_down_register(first_pud_clock_register, pin);
    clear_register_bit_at_offset(pud_register, 0, 1);
}

void bcm2835_gpio_set_pull_down(bcm2835_gpio* this, int pin)
{
    register_ptr_t pud_register = &this->memory->gppud;
    register_ptr_t first_pud_clock_register = &this->memory->gppudclk0;

    set_register_bit_at_offset(pud_register, 0, 0);
    clock_pull_up_down_register(first_pud_clock_register, pin);
    clear_register_bit_at_offset(pud_register, 0, 0);
}

void bcm2835_gpio_clear_pull_up_down(bcm2835_gpio* this, int pin)
{
    register_ptr_t pud_register = &this->memory->gppud;
    register_ptr_t first_pud_clock_register = &this->memory->gppudclk0;

    clear_register_bit_at_offset(pud_register, 0, 0);
    clear_register_bit_at_offset(pud_register, 0, 1);
    clock_pull_up_down_register(first_pud_clock_register, pin);
}

static inline void clock_pull_up_down_register(register_ptr_t pud_clock_base_register, int pin)
{
    int register_offset = get_common_register_offset(pin);
    int register_bit_offset = get_common_register_bit_offset(pin);

    set_register_bit_at_offset(pud_clock_base_register, register_offset, register_bit_offset);
    wait_n_clock_ticks(PUDCLK_WAIT_TICKS);
    clear_register_bit_at_offset(pud_clock_base_register, register_offset, register_bit_offset);
    wait_n_clock_ticks(PUDCLK_WAIT_TICKS);
}

static inline void set_register_bit_at_offset(register_ptr_t base_register, int register_offset,
                                              int bit_offset)
{
    *(base_register + register_offset) |= 1 << bit_offset;
}

static inline void clear_register_bit_at_offset(register_ptr_t base_register, int register_offset,
                                                int bit_offset)
{
    *(base_register + register_offset) &= ~(1 << bit_offset);
}

static inline int get_common_register_offset(int pin)
{
    return (pin - (pin % 32)) / 32;
}

static inline int get_common_register_bit_offset(int pin)
{
    return pin % 32;
}

static inline void wait_n_clock_ticks(unsigned int n_ticks)
{
    clock_t cur = clock();
    clock_t next = cur + n_ticks;
    while (cur != next)
        cur = clock();
}
