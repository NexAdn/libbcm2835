#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <unistd.h>

#include "bcm2835/bcm2835_peripheral.h"

#define BCM2835_BLOCK_SIZE 4096

static int open_dev_mem();
static inline void* map_memory(void* map, int fd, unsigned long addr_p);

static inline void unmap_memory(void* map);
static inline void close_dev_mem(int fd);

bcm2835_peripheral* bcm2835_peripheral_create()
{
    bcm2835_peripheral* this = malloc(sizeof(bcm2835_peripheral));
    return this;
}

int bcm2835_peripheral_open(bcm2835_peripheral* this)
{
    this->fd = open_dev_mem();
    if (this->fd < 0) {
        fprintf(stderr,
                "bcm2835_peripheral_open(): Failed to get file descriptor "
                "for /dev/mem\n");
        return -1;
    }

    this->map = map_memory(this->map, this->fd, this->addr_p);
    this->addr = (volatile uint32_t*) this->map;

    return 0;
}

static int open_dev_mem()
{
    int fd = -1;
    if ((fd = open("/dev/mem", O_RDWR | O_SYNC)) < 0)
        fprintf(stderr, "open_dev_mem(): Failed to open /dev/mem\n");
    return fd;
}

static inline void* map_memory(void* map, int fd, unsigned long addr_p)
{
    map = mmap(NULL, BCM2835_BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, addr_p);
    return map;
}

void bcm2835_peripheral_close(bcm2835_peripheral* this)
{
    unmap_memory(this->map);
    close_dev_mem(this->fd);

    free(this);
}

static inline void unmap_memory(void* map)
{
    munmap(map, BCM2835_BLOCK_SIZE);
}

static inline void close_dev_mem(int fd)
{
    close(fd);
}

void bcm2835_peripheral_destroy(bcm2835_peripheral* this)
{
    free(this);
}

void bcm2835_peripheral_write_reg(bcm2835_peripheral* this, size_t register_offset, uint32_t value)
{
    *(this->addr + register_offset) = value;
}

uint32_t bcm2835_peripheral_read_reg(bcm2835_peripheral* this, size_t register_offset)
{
    return *(this->addr + register_offset);
}
