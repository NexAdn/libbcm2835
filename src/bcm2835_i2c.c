#include <stdio.h>
#include <stdlib.h>

#include "bcm2835/bcm2835_i2c.h"
#include "bcm2835/bcm2835_peripheral.h"

#define BCM2835_REG_C_I2CEN (1 << 15)
#define BCM2835_REG_C_INTR (1 << 10)
#define BCM2835_REG_C_INTT (1 << 9)
#define BCM2835_REG_C_INTD (1 << 8)
#define BCM2835_REG_C_ST (1 << 7)
#define BCM2835_REG_C_CLEAR (1 << 4)
#define BCM2835_REG_C_READ (1 << 0)

#define BCM2835_REG_S_CLKT (1 << 9)
#define BCM2835_REG_S_ERR (1 << 8)
#define BCM2835_REG_S_RXF (1 << 7)
#define BCM2835_REG_S_TXE (1 << 6)
#define BCM2835_REG_S_RXD (1 << 5)
#define BCM2835_REG_S_TXD (1 << 4)
#define BCM2835_REG_S_RXR (1 << 3)
#define BCM2835_REG_S_TXW (1 << 2)
#define BCM2835_REG_S_DONE (1 << 1)
#define BCM2835_REG_S_TA (1 << 0)

#define BCM2835_REG_DEL_FEDL (1 << 16)
#define BCM2835_REG_DEL_REDL (1 << 0)

static const int BCM_CORE_CLOCK_KHZ = 150000;

bcm2835_i2c* bcm2835_i2c_create(BCM2835_BSC bsc_master)
{
    bcm2835_i2c* this = malloc(sizeof(bcm2835_i2c));
    this->dev = bcm2835_peripheral_create();
    this->dev->addr_p = BCM2835_PERIPHERAL_BASE + bsc_master;

    return this;
}

int bcm2835_i2c_open(bcm2835_i2c* this)
{
    if (bcm2835_peripheral_open(this->dev) != 0) {
        fprintf(stderr, "bcm2835_i2c_open(): Failed to open peripheral\n");
        return -1;
    }
    this->memory = (volatile bcm2835_i2c_memory*) this->dev->addr;
    return 0;
}

void bcm2835_i2c_configure(bcm2835_i2c* this, bcm2835_i2c_settings* settings)
{
    this->memory->clock_divider = settings->clock_divider;
    this->memory->data_delay = (settings->falling_edge_delay << 2) | (settings->rising_edge_delay);
}

uint16_t bcm2835_i2c_get_clock_div(uint32_t scl_kHz)
{
    return (BCM_CORE_CLOCK_KHZ / scl_kHz);
}

void bcm2835_i2c_close(bcm2835_i2c* this)
{
    this->memory = NULL;
    bcm2835_peripheral_close(this->dev);
}

void bcm2835_i2c_destroy(bcm2835_i2c* this)
{
    bcm2835_peripheral_destroy(this->dev);
    free(this);
}

void bcm2835_i2c_write7(bcm2835_i2c* dev, uint8_t addr, void* data, size_t len)
{
    // TODO
}

void* bcm2835_i2c_read7(bcm2835_i2c* dev, uint8_t addr, void* dataOut, size_t len)
{
    // TODO
    return NULL;
}
