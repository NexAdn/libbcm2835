#ifndef BCM2835_I2C_H__
#define BCM2835_I2C_H__
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

typedef enum BCM2835_BSC_
{
    BCM2835_BSC0 = 0x205000,
    BCM2835_BSC1 = 0x804000
} BCM2835_BSC;

typedef struct bcm2835_peripheral_ bcm2835_peripheral;
typedef struct bcm2835_i2c_memory_ bcm2835_i2c_memory;

typedef struct bcm2835_i2c_
{
    bcm2835_peripheral* dev;
    volatile bcm2835_i2c_memory* memory;
} bcm2835_i2c;

typedef struct bcm2835_i2c_memory_
{
    uint32_t control;
    uint32_t status;
    uint32_t data_length;
    uint32_t slave_address;
    uint32_t data_fifo;
    uint32_t clock_divider;
    uint32_t data_delay;
    uint32_t clock_stretch_timeout;
} bcm2835_i2c_memory;

typedef struct bcm2835_i2c_settings_
{
    uint16_t clock_divider;
    uint16_t falling_edge_delay;
    uint16_t rising_edge_delay;
    uint16_t clock_strech_timeout;
} bcm2835_i2c_settings;

bcm2835_i2c* bcm2835_i2c_create(BCM2835_BSC bsc_master);

int bcm2835_i2c_open(bcm2835_i2c* dev);

void bcm2835_i2c_close(bcm2835_i2c* dev);

void bcm2835_i2c_destroy(bcm2835_i2c* dev);

void bcm2835_i2c_configure(bcm2835_i2c* dev, bcm2835_i2c_settings* settings);

uint16_t bcm2835_i2c_get_clock_div(uint32_t scl_kHz);

void bcm2835_i2c_write7(bcm2835_i2c* dev, uint8_t addr, void* data, size_t len);

void* bcm2835_i2c_read7(bcm2835_i2c* dev, uint8_t addr, void* dataOot, size_t len);

#ifdef __cplusplus
}
#endif

#endif // BCM2835_I2C_H__
