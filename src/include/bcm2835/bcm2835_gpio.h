#ifndef BCM2835_GPIO_H__
#define BCM2835_GPIO_H__
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#include "bcm2835/bcm2835_peripheral.h"

#define BCM2835_GPIO (BCM2835_PERIPHERAL_BASE + 0x200000)

typedef enum BCM2835_GPIO_FUNCTION_
{
    BCM2835_GPIO_FUNCTION_INPUT = 0x0,
    BCM2835_GPIO_FUNCTION_OUTPUT = 0x1,
    BCM2835_GPIO_FUNCTION_ALT0 = 0x4,
    BCM2835_GPIO_FUNCTION_ALT1 = 0x5,
    BCM2835_GPIO_FUNCTION_ALT2 = 0x6,
    BCM2835_GPIO_FUNCTION_ALT3 = 0x7,
    BCM2835_GPIO_FUNCTION_ALT4 = 0x3,
    BCM2835_GPIO_FUNCTION_ALT5 = 0x2
} BCM2835_GPIO_FUNCTION;

typedef enum BCM2835_GPIO_LEVEL_
{
    BCM2835_GPIO_LEVEL_HIGH = 0x1,
    BCM2835_GPIO_LEVEL_LOW = 0x0
} BCM2835_GPIO_LEVEL;

typedef struct bcm2835_gpio_memory_ bcm2835_gpio_memory;

typedef struct bcm2835_gpio_
{
    bcm2835_peripheral* peripheral;
    volatile bcm2835_gpio_memory* memory;
} bcm2835_gpio;

typedef struct bcm2835_gpio_memory_
{
    uint32_t gpfsel0;
    uint32_t gpfsel1;
    uint32_t gpfsel2;
    uint32_t gpfsel3;
    uint32_t gpfsel4;
    uint32_t gpfsel5;
    uint32_t blank0_;
    uint32_t gpset0;
    uint32_t gpset1;
    uint32_t blank1_;
    uint32_t gpclr0;
    uint32_t gpclr1;
    uint32_t blank2_;
    uint32_t gplev0;
    uint32_t gplev1;
    uint32_t blank3_;
    uint32_t gpeds0;
    uint32_t gpeds1;
    uint32_t blank4_;
    uint32_t gpren0;
    uint32_t gpren1;
    uint32_t blank5_;
    uint32_t gpfen0;
    uint32_t gpfen1;
    uint32_t blank6_;
    uint32_t gphen0;
    uint32_t gphen1;
    uint32_t blank7_;
    uint32_t gplen0;
    uint32_t gplen1;
    uint32_t blank8_;
    uint32_t gparen0;
    uint32_t gparen1;
    uint32_t blank9_;
    uint32_t gpafen0;
    uint32_t gpafen1;
    uint32_t blank10_;
    uint32_t gppud;
    uint32_t gppudclk0;
    uint32_t gppudclk1;
    // Missing: 16 + 4 bit remaining reserved/test
} bcm2835_gpio_memory;

bcm2835_gpio* bcm2835_gpio_create();
int bcm2835_gpio_open(bcm2835_gpio* dev);

void bcm2835_gpio_close(bcm2835_gpio* dev);
void bcm2835_gpio_destroy(bcm2835_gpio* dev);

void bcm2835_gpio_select_function(bcm2835_gpio* dev, int pin, BCM2835_GPIO_FUNCTION function);

void bcm2835_gpio_set_output(bcm2835_gpio* dev, int pin, BCM2835_GPIO_LEVEL level);
void bcm2835_gpio_set_output_high(bcm2835_gpio* dev, int pin);
void bcm2835_gpio_set_output_low(bcm2835_gpio* dev, int pin);

BCM2835_GPIO_LEVEL bcm2835_gpio_get_input(bcm2835_gpio* dev, int pin);

void bcm2835_gpio_set_pull_up(bcm2835_gpio* dev, int pin);
void bcm2835_gpio_set_pull_down(bcm2835_gpio* dev, int pin);
void bcm2835_gpio_clear_pull_up_down(bcm2835_gpio* dev, int pin);

#ifdef __cplusplus
}
#endif

#endif // BCM2835_GPIO_H__
