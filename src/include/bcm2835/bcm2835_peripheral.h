#ifndef BCM2835_PERIPHERAL_H__
#define BCM2835_PERIPHERAL_H__
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

#define BCM2835_PERIPHERAL_BASE 0x20000000

typedef struct bcm2835_peripheral_
{
    int fd;
    unsigned long addr_p;
    void* map;
    volatile uint32_t* addr;
} bcm2835_peripheral;

bcm2835_peripheral* bcm2835_peripheral_create();

int bcm2835_peripheral_open(bcm2835_peripheral* dev);

void bcm2835_peripheral_close(bcm2835_peripheral* dev);

void bcm2835_peripheral_destroy(bcm2835_peripheral* dev);

void bcm2835_peripheral_write_reg(bcm2835_peripheral* dev, size_t register_offset, uint32_t value);

uint32_t bcm2835_peripheral_read_reg(bcm2835_peripheral* dev, size_t register_offset);

#ifdef __cplusplus
}
#endif

#endif // BCM2835_PERIPHERAL_H__
