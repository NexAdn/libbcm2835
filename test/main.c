#include <cgreen/cgreen.h>

TestSuite* bcm2835_gpio_suite();
TestSuite* bcm2835_i2c_suite();
TestSuite* bcm2835_peripheral_suite();

int main(int argc, char** argv)
{
    TestSuite* suite = create_test_suite();
    add_suite(suite, bcm2835_gpio_suite());
    add_suite(suite, bcm2835_i2c_suite());
    add_suite(suite, bcm2835_peripheral_suite());

    if (argc > 1) {
        return run_single_test(suite, argv[1], create_text_reporter());
    } else {
        return run_test_suite(suite, create_text_reporter());
    }
}
