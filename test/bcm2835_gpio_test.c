#include <string.h>

#include <cgreen/cgreen.h>

#include <bcm2835/bcm2835_gpio.h>

static bcm2835_gpio* dev;

Describe(bcm2835_gpio);
BeforeEach(bcm2835_gpio)
{
    dev = bcm2835_gpio_create();
    dev->memory = malloc(sizeof(bcm2835_gpio_memory));
    memset((void*) dev->memory, 0, sizeof(*(dev->memory)));
}
AfterEach(bcm2835_gpio)
{
    free((void*) dev->memory);
    bcm2835_gpio_destroy(dev);
}

Ensure(bcm2835_gpio, creates)
{
    bcm2835_gpio* dev = bcm2835_gpio_create();
    assert_that(dev, is_not_null);
    assert_that(dev->peripheral, is_not_null);
}

Ensure(bcm2835_gpio, fails_open_with_normal_perms)
{
    bcm2835_gpio* dev = bcm2835_gpio_create();

    int res = bcm2835_gpio_open(dev);

    assert_not_equal(res, 0);
}

Ensure(bcm2835_gpio, selects_functions_properly)
{
    bcm2835_gpio_select_function(dev, 0, BCM2835_GPIO_FUNCTION_ALT0);
    bcm2835_gpio_select_function(dev, 9, BCM2835_GPIO_FUNCTION_ALT4);

    assert_equal(dev->memory->gpfsel0, 0x18000004);

    bcm2835_gpio_select_function(dev, 0, BCM2835_GPIO_FUNCTION_OUTPUT);

    assert_equal(dev->memory->gpfsel0, 0x18000001);
}

Ensure(bcm2835_gpio, function_select_selects_register_offsets_properly)
{
    bcm2835_gpio_select_function(dev, 15, BCM2835_GPIO_FUNCTION_ALT4);
    bcm2835_gpio_select_function(dev, 35, BCM2835_GPIO_FUNCTION_ALT4);

    assert_equal(dev->memory->gpfsel0, 0);
    assert_equal(dev->memory->gpfsel1, 0x18000);
    assert_equal(dev->memory->gpfsel3, 0x18000);
}

Ensure(bcm2835_gpio, set_output_high_works_properly)
{
    bcm2835_gpio_set_output_high(dev, 0);
    bcm2835_gpio_set_output(dev, 33, BCM2835_GPIO_LEVEL_HIGH);

    assert_equal(dev->memory->gpset0, 0x1);
    assert_equal(dev->memory->gpset1, 0x2);
}

Ensure(bcm2835_gpio, set_output_low_works_properly)
{
    bcm2835_gpio_set_output_low(dev, 0);
    bcm2835_gpio_set_output(dev, 33, BCM2835_GPIO_LEVEL_LOW);

    assert_equal(dev->memory->gpclr0, 0x1);
    assert_equal(dev->memory->gpclr1, 0x2);
}

Ensure(bcm2835_gpio, get_input_works_properly)
{
    dev->memory->gplev0 = 0xa;
    dev->memory->gplev1 = 0xa;

    assert_equal(bcm2835_gpio_get_input(dev, 0), BCM2835_GPIO_LEVEL_LOW);
    assert_equal(bcm2835_gpio_get_input(dev, 1), BCM2835_GPIO_LEVEL_HIGH);
    assert_equal(bcm2835_gpio_get_input(dev, 2), BCM2835_GPIO_LEVEL_LOW);
    assert_equal(bcm2835_gpio_get_input(dev, 3), BCM2835_GPIO_LEVEL_HIGH);
    assert_equal(bcm2835_gpio_get_input(dev, 32), BCM2835_GPIO_LEVEL_LOW);
    assert_equal(bcm2835_gpio_get_input(dev, 33), BCM2835_GPIO_LEVEL_HIGH);
    assert_equal(bcm2835_gpio_get_input(dev, 34), BCM2835_GPIO_LEVEL_LOW);
    assert_equal(bcm2835_gpio_get_input(dev, 35), BCM2835_GPIO_LEVEL_HIGH);
}

Ensure(bcm2835_gpio, sets_pull_up_properly)
{
    // TODO
    fail_test("Not implemented");
}

Ensure(bcm2835_gpio, sets_pull_down_properly)
{
    // TODO
    fail_test("Not implemented");
}

Ensure(bcm2835_gpio, clears_pull_up_down_properly)
{
    // TODO
    fail_test("Not implemented");
}

TestSuite* bcm2835_gpio_suite()
{
    TestSuite* suite = create_test_suite();
    add_test_with_context(suite, bcm2835_gpio, creates);
    add_test_with_context(suite, bcm2835_gpio, fails_open_with_normal_perms);
    add_test_with_context(suite, bcm2835_gpio, selects_functions_properly);
    add_test_with_context(suite, bcm2835_gpio, function_select_selects_register_offsets_properly);
    add_test_with_context(suite, bcm2835_gpio, set_output_high_works_properly);
    add_test_with_context(suite, bcm2835_gpio, set_output_low_works_properly);
    add_test_with_context(suite, bcm2835_gpio, get_input_works_properly);
    add_test_with_context(suite, bcm2835_gpio, sets_pull_up_properly);
    add_test_with_context(suite, bcm2835_gpio, sets_pull_down_properly);
    add_test_with_context(suite, bcm2835_gpio, clears_pull_up_down_properly);
    return suite;
}
