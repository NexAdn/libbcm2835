#include <cgreen/cgreen.h>

#include <bcm2835/bcm2835_peripheral.h>

Describe(bcm2835_peripheral);
BeforeEach(bcm2835_peripheral)
{}
AfterEach(bcm2835_peripheral)
{}

Ensure(bcm2835_peripheral, creates)
{
    bcm2835_peripheral* peripheral = bcm2835_peripheral_create();
    assert_that(peripheral, is_not_null);

    bcm2835_peripheral_destroy(peripheral);
}

Ensure(bcm2835_peripheral, write_reg_writes_correct)
{
    bcm2835_peripheral* peripheral = bcm2835_peripheral_create();
    volatile uint32_t* memory = malloc(sizeof(uint32_t) * 3);

    peripheral->addr = memory;

    bcm2835_peripheral_write_reg(peripheral, 0, 0xff00ff00);
    assert_equal(memory[0], 0xff00ff00);

    bcm2835_peripheral_write_reg(peripheral, 2, 0xff00ff00);
    assert_equal(memory[2], 0xff00ff00);
}

Ensure(bcm2835_peripheral, read_reg_reads_correct)
{
    bcm2835_peripheral* peripheral = bcm2835_peripheral_create();
    volatile uint32_t* memory = malloc(sizeof(uint32_t) * 4);

    peripheral->addr = memory;

    memory[0] = 0xdeadbeef;
    assert_equal(bcm2835_peripheral_read_reg(peripheral, 0), 0xdeadbeef);

    memory[3] = 0xdeadbeef;
    assert_equal(bcm2835_peripheral_read_reg(peripheral, 3), 0xdeadbeef);
}

TestSuite* bcm2835_peripheral_suite()
{
    TestSuite* suite = create_test_suite();
    add_test_with_context(suite, bcm2835_peripheral, creates);
    add_test_with_context(suite, bcm2835_peripheral, write_reg_writes_correct);
    add_test_with_context(suite, bcm2835_peripheral, read_reg_reads_correct);
    return suite;
}
