#include <cgreen/cgreen.h>

#include <bcm2835/bcm2835_i2c.h>
#include <bcm2835/bcm2835_peripheral.h>

Describe(bcm2835_i2c);
BeforeEach(bcm2835_i2c)
{}
AfterEach(bcm2835_i2c)
{}

Ensure(bcm2835_i2c, creates)
{
    bcm2835_i2c* dev = bcm2835_i2c_create(BCM2835_BSC0);

    assert_that(dev, is_not_null);
    assert_that(dev->dev, is_not_null);
    assert_equal(dev->dev->addr_p, BCM2835_BSC0 + BCM2835_PERIPHERAL_BASE);
}

Ensure(bcm2835_i2c, fails_open_with_normal_perms)
{
    bcm2835_i2c* dev = bcm2835_i2c_create(BCM2835_BSC0);

    int res = bcm2835_i2c_open(dev);

    assert_not_equal(res, 0);
}

Ensure(bcm2835_i2c, calculates_clock_divider_correctly)
{
    uint16_t cdiv1 = bcm2835_i2c_get_clock_div(100);
    uint16_t cdiv2 = bcm2835_i2c_get_clock_div(225);

    assert_equal(cdiv1, 1500);
    assert_equal(cdiv2, 666);
}

TestSuite* bcm2835_i2c_suite()
{
    TestSuite* suite = create_test_suite();
    add_test_with_context(suite, bcm2835_i2c, creates);
    add_test_with_context(suite, bcm2835_i2c, fails_open_with_normal_perms);
    add_test_with_context(suite, bcm2835_i2c, calculates_clock_divider_correctly);
    return suite;
}
